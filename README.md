
## Add Film Fan project to your IDE

1. Film Fan project is an android application
2. This project was created using Android Studio 2.3.3 as IDE
3. Clone the project using the command: "git clone https://mbonyi@bitbucket.org/mbonyi/film-fan.git"
Note:Make sure you are in the directory where you want to keep your project
4. android:minSdkVersion="14"
5. android:targetSdkVersion="26" 


## Generated APK file

1. When you have compiled, built and run your project without errors, go to Build>>Build APK and wait for the process to finish.
2. When the APK generation process has ended, you can find the APK file in Filmfan/app/build/outputs/apk folder
3. Internet is required to run the application since it involves a few calls to the server
