package com.sildio.filmfan;

import com.sildio.filmfan.utils.DateUtil;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * dateUtil unit test
 *
 * @author sildio
 */
public class DateUtilUnitTest {

    @Test
    public void getYearFromStringDateCorrect() throws Exception {
        assertEquals(DateUtil.getYearFromDate("2016-07-23"), 2016);
    }
}