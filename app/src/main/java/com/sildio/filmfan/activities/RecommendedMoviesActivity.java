package com.sildio.filmfan.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sildio.filmfan.R;
import com.sildio.filmfan.adapters.RecommendedMovieAdapter;
import com.sildio.filmfan.helpers.CoreApiHelper;
import com.sildio.filmfan.models.Movie;
import com.sildio.filmfan.utils.CoreApiKeys;
import com.sildio.filmfan.utils.DateUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.sildio.filmfan.BuildConfig.API_KEY;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_GUEST_SESSION_ID;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_KEY;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_STATUS_MESSAGE;
import static com.sildio.filmfan.utils.CoreUrlKeys.CORE_API_URL_PATH_MOVIE;
import static com.sildio.filmfan.utils.CoreUrlKeys.IMAGE_URL_PATH;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_ID;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_OVERVIEW;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_POSTER_PATH;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_RELEASE_DATE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_TITLE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_VALUE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_VOTE_AVERAGE;


/**
 * Activity that lists all recommendations (movies) to the selected movie
 *
 * Created by sildio on 9/5/18.
 */
public class RecommendedMoviesActivity extends AppCompatActivity {
    ListView list;
    EditText txtSearchKey;
    RecommendedMovieAdapter adapter;
    public static  ArrayList<Movie> movies=null;
    public static Context context;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    ProgressDialog progressDialog = null;
    MoviesAsyncTask moviesAsyncTask = null;
    RateAsyncTask rateAsyncTask=null;
    CoreApiHelper coreApisHelper = null;
    RatingBar ratingBar;
    long selectedMovieId;
    String selectedMovieTitle;

    /**
     * Function to initiate the activity GUI
     * @param savedInstanceState
     * @return
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_similar_movies);
        txtSearchKey=(EditText) findViewById(R.id.txt_search);
        context = getApplicationContext();
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor=settings.edit();
        if (getIntent().getLongExtra("selectedMovieId",0)!=0) {
            selectedMovieId=getIntent().getLongExtra("selectedMovieId",0);
        }
        if (getIntent().getStringExtra("selectedMovieTitle")!=null) {
            selectedMovieTitle=getIntent().getStringExtra("selectedMovieTitle");
        }
        getSupportActionBar().setTitle(getString(R.string.prompt_recommendations));
        getSupportActionBar().setSubtitle(selectedMovieTitle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try {
            showProgress(true);
            movies=new ArrayList<>();
            moviesAsyncTask = new MoviesAsyncTask();
            moviesAsyncTask.execute(CORE_API_URL_PATH_MOVIE+"/"+selectedMovieId+"/recommendations?"+CORE_API_KEY+"="+ API_KEY+"&page=1");
        } catch (Exception e) {
            DialogInterface.OnClickListener dialogClickListenerExit = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder exit = new AlertDialog.Builder(context);
            exit.setMessage(Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'>"+getString(R.string.error_connection)+"</font>"))
                    .setPositiveButton(Html
                            .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                    + "'><b>"+getString(R.string.label_ok)+"</b></font>"), dialogClickListenerExit).show();

            Log.d("Attendances", e.getLocalizedMessage());
            e.printStackTrace();
        }

        list = (ListView) findViewById(R.id.list);
        /**************** Create Custom Adapter *********/
        adapter = new RecommendedMovieAdapter(RecommendedMoviesActivity.this, movies);
        list.setAdapter(adapter);
        filterMovies();
    }

    /**
     * MoviesAsyncTask class that calls recommended movies
     *
     * @author sildio
     *
     */
    private class MoviesAsyncTask extends
            AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            coreApisHelper=new CoreApiHelper();
            try {
                return coreApisHelper.getJsonByUrlGET(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected void onPostExecute(String result) {
            showProgress(false);
            Log.d("Result is+++++", result);
            try {
                JSONObject json = new JSONObject(result);
                if (!json.isNull(CoreApiKeys.CORE_API_RESULTS)){
                    JSONArray jsonResponse=	json.getJSONArray(CoreApiKeys.CORE_API_RESULTS);
                    if(jsonResponse.length()>0){
                        for (int i=0; i<jsonResponse.length(); i++){

                            Movie movie=new Movie();
                            JSONObject jsonMovie=jsonResponse.getJSONObject(i);

                            movie.setId(jsonMovie.getLong(CORE_API_MOVIE_ID));
                            movie.setTitle(jsonMovie.getString(CORE_API_MOVIE_TITLE));
                            movie.setReleaseDate(jsonMovie.getString(CORE_API_MOVIE_RELEASE_DATE));
                            movie.setOverview(jsonMovie.getString(CORE_API_MOVIE_OVERVIEW));
                            movie.setVoteAverage(jsonMovie.getDouble(CORE_API_MOVIE_VOTE_AVERAGE));
                            movie.setPosterPath(jsonMovie.getString(CORE_API_MOVIE_POSTER_PATH));
                            movies.add(movie);
                            adapter.notifyDataSetChanged();
                        }
                        Collections.sort(movies, new Comparator<Movie>(){
                            public int compare(Movie movie1, Movie movie2) {
                                // ## Ascending order of movie titles
                                return movie1.getTitle().compareToIgnoreCase(movie2.getTitle());
                            }
                        });
                    }
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            RecommendedMoviesActivity.this);
                    alertDialogBuilder
                            .setTitle(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+getString(R.string.label_title_failed)+"</font>"))
                            .setMessage(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+json.getString(CORE_API_STATUS_MESSAGE)+"</font>"));
                    alertDialogBuilder.setPositiveButton(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }

                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        RecommendedMoviesActivity.this);
                alertDialogBuilder
                        .setMessage(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'>"+getString(R.string.error_connection)+"</font>"));
                alertDialogBuilder.setPositiveButton(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }

                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            moviesAsyncTask = null;
            showProgress(false);
        }
    }

    /**
     * RateAsyncTask class that allows the user to rate the movie
     *
     * @author sildio
     *
     */
    private class RateAsyncTask extends
            AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            coreApisHelper=new CoreApiHelper();
            JSONObject data = rateMovieRequestData();
            try {
                return coreApisHelper.getJsonByUrlPOST(urls[0],data);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected void onPostExecute(String result) {
            showProgressRating(false);
            Log.d("Result is+++++", result);
            try {
                JSONObject json = new JSONObject(result);
                if (!json.isNull(CORE_API_STATUS_MESSAGE)){
                    Toast.makeText(context,json.getString(CORE_API_STATUS_MESSAGE), Toast.LENGTH_LONG ).show();
                }

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        RecommendedMoviesActivity.this);
                alertDialogBuilder
                        .setMessage(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'>"+getString(R.string.error_connection)+"</font>"));
                alertDialogBuilder.setPositiveButton(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }

                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            rateAsyncTask = null;
            showProgressRating(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showProgress(false);
    }

    /**
     * Function that shows the progress UI while loading the movies from the server
     *
     * @param show
     * @return
     *
     */
    private void showProgress(final boolean show) {
        if (show) {
            progressDialog = ProgressDialog.show(this, null,Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'><b>"+getString(R.string.message_loading_movies)+"...</b></font>"), true, false);
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }
    /**
     * Function that shows the progress UI while rating movie
     *
     * @param show
     * @return
     *
     */
    private void showProgressRating(final boolean show) {
        if (show) {
            progressDialog = ProgressDialog.show(this, null,Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'><b>"+getString(R.string.message_rating_movie)+"...</b></font>"), true, false);
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

    /**
     * Function filters movies according to the values entered in the search text
     *
     * @param
     * @return
     *
     */
    public void filterMovies(){
        txtSearchKey.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (txtSearchKey.getText().toString().isEmpty()
                        || txtSearchKey.getText().toString().equals("")){
                    /**************** Create Custom Adapter *********/
                    adapter = new RecommendedMovieAdapter(RecommendedMoviesActivity.this,
                            movies);
                    list.setAdapter(adapter);
                }		//
                else
                {
                    ArrayList<Movie> filteredList=new ArrayList<Movie>();
                    for(int i=0; i<movies.size(); i++)
                    {
                        if (movies.get(i).getTitle().toLowerCase().contains(txtSearchKey.getText().toString().toLowerCase()))
                        {
                            filteredList.add(movies.get(i));

                        }

                    }
                    /**************** Create Custom Adapter *********/
                    adapter = new RecommendedMovieAdapter(RecommendedMoviesActivity.this,
                            filteredList);
                    list.setAdapter(adapter);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
            }
        });
    }

    /**
     *
     * Function that show selected movie details
     *
     * @param movie
     * @return
     */
    @SuppressLint("InflateParams")
    public void showMovieDialog(Movie movie) {
        final Movie selectedMovie=movie;
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(
                R.layout.layout_movie_details, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                RecommendedMoviesActivity.this);
        alertDialogBuilder.setView(promptView);
        TextView txtTitle=(TextView) promptView.findViewById(R.id.txt_movie_title);
        TextView txtOverview=(TextView) promptView.findViewById(R.id.txt_overview);
        TextView txtYear=(TextView) promptView.findViewById(R.id.txt_release_year);
        TextView txtRating=(TextView) promptView.findViewById(R.id.txt_rating);
        ImageView imgMovie=(ImageView) promptView.findViewById(R.id.img_poster);
        Button btnSubmit=(Button) promptView.findViewById(R.id.btn_submit);
        ratingBar=(RatingBar) promptView.findViewById(R.id.rating_bar);
        txtTitle.setText(selectedMovie.getTitle());
        txtOverview.setText(selectedMovie.getOverview());
        txtYear.setText(DateUtil.getYearFromDate(selectedMovie.getReleaseDate())+"");
        txtRating.setText(selectedMovie.getVoteAverage()+" ");
        Picasso.with(context).load(IMAGE_URL_PATH+movie.getPosterPath()).placeholder(R.mipmap.movie_placeholder).into(imgMovie);
        final AlertDialog alert = alertDialogBuilder.create();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressRating(true);
                rateAsyncTask = new RateAsyncTask();
                rateAsyncTask.execute(CORE_API_URL_PATH_MOVIE+"/"+selectedMovie.getId()+"/rating?"+CORE_API_KEY+"="+ API_KEY+"&"+CORE_API_GUEST_SESSION_ID+"="+settings.getString("guest_session_id",null));
                getSupportActionBar().setTitle(getString(R.string.prompt_recommendations));
                getSupportActionBar().setSubtitle(selectedMovie.getTitle());
                alert.dismiss();
            }
        });
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false);

        alertDialogBuilder.setPositiveButton(Html.fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create an alert dialog
        // create an alert dialog
        AlertDialog alert1 = alertDialogBuilder.create();
        alert1.show();

        int dividerId = alert1.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        View divider = alert1.findViewById(dividerId);
        if(divider!=null)
            divider.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        Button theButton = alert1.getButton(DialogInterface.BUTTON_POSITIVE);
        theButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    /**
     * Function that returns the JSON data to be sent in the rate movie request
     *
     * @param
     * @return json
     */
    public JSONObject rateMovieRequestData(){
        JSONObject json = new JSONObject();
        try {
            json.put(CORE_API_MOVIE_VALUE, ratingBar.getRating());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
