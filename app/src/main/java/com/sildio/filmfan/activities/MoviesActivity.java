package com.sildio.filmfan.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import com.sildio.filmfan.R;
import com.sildio.filmfan.adapters.MovieAdapter;
import com.sildio.filmfan.helpers.CoreApiHelper;
import com.sildio.filmfan.models.Movie;
import com.sildio.filmfan.utils.CoreApiKeys;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.sildio.filmfan.BuildConfig.API_KEY;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_GUEST_SESSION_ID;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_KEY;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_RESULTS;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_STATUS_MESSAGE;
import static com.sildio.filmfan.utils.CoreUrlKeys.CORE_API_URL_PATH_AUTHENTICATION;
import static com.sildio.filmfan.utils.CoreUrlKeys.CORE_API_URL_PATH_MOVIE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_ID;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_OVERVIEW;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_POSTER_PATH;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_RELEASE_DATE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_TITLE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_VOTE_AVERAGE;

/**
 * Activity that lists all the movies that are now playing
 *
 * Created by sildio on 9/5/18.
 */
public class MoviesActivity extends AppCompatActivity {
    ListView list;
    EditText txtSearchKey;
    MovieAdapter adapter;
    Button btnMyFavourites;
    public static  ArrayList<Movie> movies=null;
    public static Context context;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    ProgressDialog progressDialog = null;
    GuestSessionIdAsyncTask guestSessionIdAsyncTask=null;
    MoviesAsyncTask moviesAsyncTask = null;
    CoreApiHelper coreApisHelper = null;
    RatingBar ratingBar;

    /**
     * Function to initiate the activity GUI
     * @param savedInstanceState
     * @return
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        txtSearchKey=(EditText) findViewById(R.id.txt_search);
        btnMyFavourites=(Button) findViewById(R.id.btn_my_favourites);
        context = getApplicationContext();
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor=settings.edit();
        try {
            guestSessionIdAsyncTask = new GuestSessionIdAsyncTask();
            guestSessionIdAsyncTask.execute(CORE_API_URL_PATH_AUTHENTICATION+"/guest_session/new?"+CORE_API_KEY+"="+ API_KEY);
            movies=new ArrayList<>();
            showProgress(true);
            moviesAsyncTask = new MoviesAsyncTask();
            moviesAsyncTask.execute(CORE_API_URL_PATH_MOVIE+"/now_playing?"+CORE_API_KEY+"="+API_KEY+"&page=1");
        } catch (Exception e) {
            DialogInterface.OnClickListener dialogClickListenerExit = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder exit = new AlertDialog.Builder(context);
            exit.setMessage(Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'>"+getString(R.string.error_connection)+"</font>"))
                    .setPositiveButton(Html
                            .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                    + "'><b>"+getString(R.string.label_ok)+"</b></font>"), dialogClickListenerExit).show();

            Log.d("Attendances", e.getLocalizedMessage());
            e.printStackTrace();
        }
        btnMyFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, FavouritesActivity.class);
                startActivity(intent);
            }
        });
        list = (ListView) findViewById(R.id.list);

        /**************** Create Custom Adapter *********/
        adapter = new MovieAdapter(MoviesActivity.this,
                movies);
        list.setAdapter(adapter);
        filterMovies();
    }

    /**
     * MoviesAsyncTask class that calls the now playing movies
     *
     * @author sildio
     *
     */
    private class MoviesAsyncTask extends
            AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            coreApisHelper=new CoreApiHelper();
            try {
                return coreApisHelper.getJsonByUrlGET(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected void onPostExecute(String result) {
            showProgress(false);
            Log.d("Result is+++++", result);
            try {
                JSONObject json = new JSONObject(result);
                if (!json.isNull(CORE_API_RESULTS)){
                    JSONArray jsonResponse=	json.getJSONArray(CORE_API_RESULTS);
                    if(jsonResponse.length()>0){
                        for (int i=0; i<jsonResponse.length(); i++){

                            Movie movie=new Movie();
                            JSONObject jsonMovie=jsonResponse.getJSONObject(i);

                            movie.setId(jsonMovie.getLong(CORE_API_MOVIE_ID));
                            movie.setTitle(jsonMovie.getString(CORE_API_MOVIE_TITLE));
                            movie.setReleaseDate(jsonMovie.getString(CORE_API_MOVIE_RELEASE_DATE));
                            movie.setOverview(jsonMovie.getString(CORE_API_MOVIE_OVERVIEW));
                            movie.setVoteAverage(jsonMovie.getDouble(CORE_API_MOVIE_VOTE_AVERAGE));
                            movie.setPosterPath(jsonMovie.getString(CORE_API_MOVIE_POSTER_PATH));
                            movies.add(movie);
                            adapter.notifyDataSetChanged();
                        }  Collections.sort(movies, new Comparator<Movie>(){
                            public int compare(Movie movie1, Movie movie2) {
                                // ## Ascending order of movie titles
                                return movie1.getTitle().compareToIgnoreCase(movie2.getTitle());
                            }
                        });
                    }
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            MoviesActivity.this);
                    alertDialogBuilder
                            .setTitle(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+getString(R.string.label_title_failed)+"</font>"))
                            .setMessage(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+json.getString(CORE_API_STATUS_MESSAGE)+"</font>"));
                    alertDialogBuilder.setPositiveButton(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }

                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MoviesActivity.this);
                alertDialogBuilder
                        .setMessage(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'>"+getString(R.string.error_connection)+"</font>"));
                alertDialogBuilder.setPositiveButton(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }

                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            moviesAsyncTask = null;
            showProgress(false);
        }
    }

    /**
     * GuestSessionIdAsyncTask class that allows to get the guest session
     *
     * @author sildio
     *
     */
    private class GuestSessionIdAsyncTask extends
            AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            coreApisHelper=new CoreApiHelper();
            try {
                return coreApisHelper.getJsonByUrlGET(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                if (!json.isNull(CoreApiKeys.CORE_API_GUEST_SESSION_ID)){
                    editor.putString("guest_session_id",json.getString(CORE_API_GUEST_SESSION_ID)).apply();
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            MoviesActivity.this);
                    alertDialogBuilder
                            .setTitle(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+getString(R.string.label_title_failed)+"</font>"))
                            .setMessage(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+json.getString(CORE_API_STATUS_MESSAGE)+"</font>"));
                    alertDialogBuilder.setPositiveButton(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }

                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MoviesActivity.this);
                alertDialogBuilder
                        .setMessage(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'>"+getString(R.string.error_connection)+"</font>"));
                alertDialogBuilder.setPositiveButton(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }

                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            guestSessionIdAsyncTask = null;
            showProgress(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showProgress(false);
    }
    /**
     * Function that shows the progress UI while loading movies
     *
     * @param show
     * @return
     *
     */
    private void showProgress(final boolean show) {
        if (show) {
            progressDialog = ProgressDialog.show(this, null,Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'><b>"+getString(R.string.message_loading_movies)+"...</b></font>"), true, false);
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

    /**
     * Function filters movies according to the values entered in the search text
     *
     * @param
     * @return
     *
     */
    public void filterMovies(){
        txtSearchKey.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (txtSearchKey.getText().toString().isEmpty()
                        || txtSearchKey.getText().toString().equals("")){
                    /**************** Create Custom Adapter *********/
                    adapter = new MovieAdapter(MoviesActivity.this,
                            movies);
                    list.setAdapter(adapter);
                }		//
                else
                {
                    ArrayList<Movie> filteredList=new ArrayList<Movie>();
                    for(int i=0; i<movies.size(); i++)
                    {
                        if (movies.get(i).getTitle().toLowerCase().contains(txtSearchKey.getText().toString().toLowerCase()))
                        {
                            filteredList.add(movies.get(i));

                        }

                    }
                    /**************** Create Custom Adapter *********/
                    adapter = new MovieAdapter(MoviesActivity.this,
                            filteredList);
                    list.setAdapter(adapter);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
            }
        });
    }

}
