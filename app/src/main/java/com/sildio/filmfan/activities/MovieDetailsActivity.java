package com.sildio.filmfan.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.sildio.filmfan.R;
import com.sildio.filmfan.helpers.CoreApiHelper;
import com.sildio.filmfan.models.Genre;
import com.sildio.filmfan.models.Movie;
import com.sildio.filmfan.utils.DateUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.sildio.filmfan.BuildConfig.API_KEY;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_GUEST_SESSION_ID;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_KEY;
import static com.sildio.filmfan.utils.CoreApiKeys.CORE_API_STATUS_MESSAGE;
import static com.sildio.filmfan.utils.CoreUrlKeys.CORE_API_URL_PATH_MOVIE;
import static com.sildio.filmfan.utils.CoreUrlKeys.IMAGE_URL_PATH;
import static com.sildio.filmfan.utils.GenreKey.CORE_API_GENRE_ID;
import static com.sildio.filmfan.utils.GenreKey.CORE_API_GENRE_NAME;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_GENRES;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_ID;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_OVERVIEW;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_POSTER_PATH;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_RELEASE_DATE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_TITLE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_VALUE;
import static com.sildio.filmfan.utils.MovieKey.CORE_API_MOVIE_VOTE_AVERAGE;

/**
 * Activity that displays the details of a selected movie
 *
 * Note: there is a possibility of avoiding making a new call to the server but information like "genres" are missing
 * in the previous request (list)
 *
 *
 * Created by sildio on 9/5/18.
 */
public class MovieDetailsActivity extends AppCompatActivity {
    TextView txtTitle;
    TextView txtOverview;
    TextView txtYear;
    TextView txtRating;
    ImageView imgMovie;
    Button btnSubmit;
    Button btnRecommendations;
    RatingBar ratingBar;
    ListView listViewGenre;
    ArrayAdapter<Genre> adapter=null;
    CoreApiHelper coreApisHelper = null;
    MovieDetailsAsyncTask movieDetailsAsyncTask = null;
    RateAsyncTask rateAsyncTask=null;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    ProgressDialog progressDialog = null;
    long selectedMovieId;
    String selectedMovieTitle;
    Context context;
    ArrayList<Genre> genres=null;

    /**
     * Function to initiate the activity GUI
     * @param savedInstanceState
     * @return
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        context=getApplicationContext();
        context = getApplicationContext();
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor=settings.edit();
        txtTitle=(TextView) findViewById(R.id.txt_movie_title);
        txtOverview=(TextView) findViewById(R.id.txt_overview);
        txtYear=(TextView) findViewById(R.id.txt_release_year);
        txtRating=(TextView)findViewById(R.id.txt_rating);
        imgMovie=(ImageView)findViewById(R.id.img_poster);
        listViewGenre=(ListView)findViewById(R.id.list_genres);
        btnSubmit=(Button)findViewById(R.id.btn_submit);
        btnRecommendations=(Button) findViewById(R.id.btn_check_recommendations);
        ratingBar=(RatingBar) findViewById(R.id.rating_bar);
        if (getIntent().getLongExtra("selectedMovieId",0)!=0) {
            selectedMovieId=getIntent().getLongExtra("selectedMovieId",0);
        }
        if (getIntent().getStringExtra("selectedMovieTitle")!=null) {
            selectedMovieTitle=getIntent().getStringExtra("selectedMovieTitle");
        }
        getSupportActionBar().setTitle(getString(R.string.alert_movie_detail));
        getSupportActionBar().setSubtitle(selectedMovieTitle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnRecommendations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent (MovieDetailsActivity.this, RecommendedMoviesActivity.class);
                intent.putExtra("selectedMovieId", selectedMovieId);
                intent.putExtra("selectedMovieTitle",selectedMovieTitle);
                startActivity(intent);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressRating(true);
                rateAsyncTask = new RateAsyncTask();
                rateAsyncTask.execute(CORE_API_URL_PATH_MOVIE+"/"+selectedMovieId+"/rating?"+CORE_API_KEY+"="+ API_KEY+"&"+CORE_API_GUEST_SESSION_ID+"="+settings.getString("guest_session_id",null));

            }
        });
        showProgress(true);
        movieDetailsAsyncTask = new MovieDetailsAsyncTask();
        movieDetailsAsyncTask.execute(CORE_API_URL_PATH_MOVIE+"/"+selectedMovieId+"?"+CORE_API_KEY+"="+API_KEY);
    }

    /**
     * MovieDetailsAsyncTask class that calls the get movie details
     *
     * @author sildio
     *
     */
    private class MovieDetailsAsyncTask extends
            AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            coreApisHelper=new CoreApiHelper();
            try {
                return coreApisHelper.getJsonByUrlGET(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected void onPostExecute(String result) {
            showProgress(false);
            try {
                JSONObject json = new JSONObject(result);
                if (!json.isNull(CORE_API_MOVIE_ID)){
                    Movie movie=new Movie();
                    movie.setId(json.getLong(CORE_API_MOVIE_ID));
                    if (!json.isNull(CORE_API_MOVIE_TITLE))
                    movie.setTitle(json.getString(CORE_API_MOVIE_TITLE));
                    if (!json.isNull(CORE_API_MOVIE_OVERVIEW))
                    movie.setOverview(json.getString(CORE_API_MOVIE_OVERVIEW));
                    if (!json.isNull(CORE_API_MOVIE_VOTE_AVERAGE))
                    movie.setVoteAverage(json.getDouble(CORE_API_MOVIE_VOTE_AVERAGE));
                    if (!json.isNull(CORE_API_MOVIE_RELEASE_DATE))
                    movie.setReleaseDate(json.getString(CORE_API_MOVIE_RELEASE_DATE));
                    if (!json.isNull(CORE_API_MOVIE_POSTER_PATH))
                     movie.setPosterPath(json.getString(CORE_API_MOVIE_POSTER_PATH));
                    Picasso.with(context).load(IMAGE_URL_PATH+movie.getPosterPath()).placeholder(R.mipmap.movie_placeholder).into(imgMovie);
                    if (!json.isNull(CORE_API_MOVIE_GENRES)){
                        genres =new ArrayList<>();
                            JSONArray jsonGenres=	json.getJSONArray(CORE_API_MOVIE_GENRES);
                            if(jsonGenres.length()>0){
                                for (int i=0; i<jsonGenres.length(); i++){
                                    Genre genre=new Genre();
                                    JSONObject jsonGenre=jsonGenres.getJSONObject(i);
                                    genre.setId(jsonGenre.getLong(CORE_API_GENRE_ID));
                                    genre.setName(jsonGenre.getString(CORE_API_GENRE_NAME));
                                    genres.add(genre);
                                }
                                movie.setGenres(genres);
                            }
                        }

                    adapter=new ArrayAdapter<>(MovieDetailsActivity.this,android.R.layout.simple_list_item_1,genres);
                    listViewGenre.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    txtTitle.setText(movie.getTitle());
                    txtOverview.setText(movie.getOverview());
                    txtYear.setText(DateUtil.getYearFromDate(movie.getReleaseDate())+"");
                    txtRating.setText(movie.getVoteAverage()+" ");
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            MovieDetailsActivity.this);
                    alertDialogBuilder
                            .setTitle(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+getString(R.string.label_title_failed)+"</font>"))
                            .setMessage(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'>"+json.getString(CORE_API_STATUS_MESSAGE)+"</font>"));
                    alertDialogBuilder.setPositiveButton(Html
                                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                            + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }

                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MovieDetailsActivity.this);
                alertDialogBuilder
                        .setMessage(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'>"+getString(R.string.error_connection)+"</font>"));
                alertDialogBuilder.setPositiveButton(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }

                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            movieDetailsAsyncTask = null;
            showProgress(false);
        }
    }

    /**
     * RateAsyncTask class that allows the user to rate the movie
     *
     * @author sildio
     *
     */
    private class RateAsyncTask extends
            AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            coreApisHelper=new CoreApiHelper();
            JSONObject data = rateMovieRequestData();
            try {
                return coreApisHelper.getJsonByUrlPOST(urls[0],data);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected void onPostExecute(String result) {
            showProgressRating(false);
            Log.d("Result is+++++", result);
            try {
                JSONObject json = new JSONObject(result);
                if (!json.isNull(CORE_API_STATUS_MESSAGE)){
                    Toast.makeText(context,json.getString(CORE_API_STATUS_MESSAGE), Toast.LENGTH_LONG ).show();
                }

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MovieDetailsActivity.this);
                alertDialogBuilder
                        .setMessage(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'>"+getString(R.string.error_connection)+"</font>"));
                alertDialogBuilder.setPositiveButton(Html
                                .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                                        + "'><b>"+getString(R.string.label_ok)+"</b></font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }

                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            rateAsyncTask = null;
            showProgressRating(false);
        }
    }
    /**
     * Function that shows the progress UI while loading movie details
     *
     * @param show
     * @return
     *
     */
    private void showProgress(final boolean show) {
        if (show) {
            progressDialog = ProgressDialog.show(this, null,Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'><b>"+getString(R.string.message_loading_movie_details)+"...</b></font>"), true, false);
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

    /**
     * Function that shows the progress UI while rating movie
     *
     * @param show
     * @return
     *
     */
    private void showProgressRating(final boolean show) {
        if (show) {
            progressDialog = ProgressDialog.show(this, null,Html
                    .fromHtml("<font color='" + getResources().getColor(R.color.colorPrimary)
                            + "'><b>"+getString(R.string.message_rating_movie)+"...</b></font>"), true, false);
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }
    /**
     * Function that returns the JSON data to be sent in the rate movie request
     *
     * @param
     * @return json
     */
    public JSONObject rateMovieRequestData(){
        JSONObject json = new JSONObject();
        try {
            json.put(CORE_API_MOVIE_VALUE, ratingBar.getRating());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
