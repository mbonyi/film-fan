package com.sildio.filmfan.db;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.sildio.filmfan.helpers.SQLiteHelper;
import com.sildio.filmfan.models.Movie;

/**
 * Class for a few  possible queries to the "movie" table
 * Note: Not all of them are used for now
 *
 * Created by sildio on 9/7/18.
 */

public class MovieManager {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;
    private String[] allColumns = { "_id", "title", "release_date","overview","vote_average", "rating","poster_path" };

    /**
     * MovieManager constructor
     *
     * @param context
     *
     */
    public MovieManager(Context context) {
        dbHelper = new SQLiteHelper(context);
    }

    /**
     * Function to open the connection to the DB
     *
     * @param
     * @return
     *
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Function to close the DB connection
     *
     * @param
     * @return
     *
     */
    public void close() {
        dbHelper.close();
    }

    /**
     * Function to create the movie into the database
     *
     * @param movie
     * @return item
     *
     */
    public Movie create(Movie movie) {

        ContentValues values = new ContentValues();
        values.put("_id", movie.getId());
        values.put("title", movie.getTitle());
        values.put("release_date", movie.getReleaseDate());
        values.put("overview", movie.getOverview());
        values.put("vote_average", movie.getVoteAverage());
        values.put("rating", movie.getRating());
        values.put("poster_path", movie.getPosterPath());
        long insertId = database.insert(SQLiteHelper.TABLE_MOVIE, null, values);
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE, allColumns, "_id =" + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Movie item= cursorToItem(cursor);
        cursor.close();
        return item;
    }

    /**
     * Function to update the movie information in the database
     *
     * @param movie
     * @return updatedId
     *
     */
    public int update(Movie movie) {
        ContentValues values = new ContentValues();
        values.put("title", movie.getTitle());
        values.put("release_date", movie.getReleaseDate());
        values.put("overview", movie.getOverview());
        values.put("vote_average", movie.getVoteAverage());
        values.put("rating", movie.getRating());
        values.put("poster_path", movie.getPosterPath());
        int updateId = database.update(SQLiteHelper.TABLE_MOVIE, values, "_id ="+movie.getId(), null);
        return updateId;
    }

    /**
     * Function to delete a movie from the database
     *
     * @param id
     * @return
     *
     */
    public void delete(long id) {
        database.delete(SQLiteHelper.TABLE_MOVIE,  "_id ="+id, null);
    }

    /**
     * Function to get the list of all the movies from the DB
     *
     * @param
     * @return items
     *
     */
    public ArrayList<Movie> getAll() {
        ArrayList<Movie> items = new ArrayList<Movie>();
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Movie item = cursorToItem(cursor);
            items.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return items;
    }

    /**
     * Function to get the movie from the id
     *
     * @param id
     * @return movie
     *
     */
    public Movie getMovie(long id) {
        String[] filter = { id+""};
        Movie account = new Movie();
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                allColumns, "_id =?", filter, null, null, null);
        cursor.moveToFirst();
        account = cursorToItem(cursor);
        return account;
    }

    /**
     * Function to get the movie object from the movie title
     *
     * @param title
     * @return movie
     *
     */
    public Movie getMovieFromTitle(String title) {
        String[] filter = { title+""};
        Movie movie = new Movie();
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                allColumns, "title =?", filter, null, null, null);
        cursor.moveToFirst();
        movie = cursorToItem(cursor);
        return movie;
    }

    /**
     * Function to get all the movie titles
     *
     * @param
     * @return items
     *
     */
    public List<String> getAllMovieTitles() {
        List<String> items = new ArrayList<String>();
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Movie item = cursorToItem(cursor);
            items.add(item.getTitle());
            cursor.moveToNext();
        }
        cursor.close();
        return items;
    }

    /**
     * Function to get the ID from the movie title
     *
     * @param title
     * @return id
     *
     */
    public long getIdFromTitle(String title) {
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                new String[] {"_id"}, "title =? ", new String[] {title}, null, null, null);
        Log.i("HD","count" + cursor.getCount());
        cursor.moveToFirst();
        return cursor.getLong(0);
    }

    /**
     * Function to check whether the movie exists in the DB
     *
     * @param movieId
     * @return exists
     *
     */

    public boolean exists(long movieId){
        boolean exists = false;
        String[] filter = { movieId+""};
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                new String[]{"title"}, "_id =?", filter, null, null, null);
        Log.i("HD","count" + cursor.getCount());
        if(cursor.getCount() > 0){
            exists = true;
        }
        return exists;
    }

    /**
     * Function to get the movie title from the id
     *
     * @param id
     * @return title
     *
     */
    public String getMovieTitle(long id){
        String accountNumber = "N/A";
        String[] filter = { id+""};
        Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                new String[]{"title"}, "_id =?", filter, null, null, null);
        Log.i("HD","count" + cursor.getCount());
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            accountNumber=cursor.getString(0);
        }
        return accountNumber;
    }

    /**
     * Function to set the movie from cursor
     *
     * @param cursor
     * @return item
     *
     */
    private Movie cursorToItem(Cursor cursor) {
        Movie item = new Movie();
        item.setId(cursor.getInt(0));
        item.setTitle(cursor.getString(1));
        item.setReleaseDate(cursor.getString(2));
        item.setOverview(cursor.getString(3));
        item.setVoteAverage(cursor.getDouble(4));
        item.setRating(cursor.getDouble(5));
        item.setPosterPath(cursor.getString(6));
        return item;
    }
}