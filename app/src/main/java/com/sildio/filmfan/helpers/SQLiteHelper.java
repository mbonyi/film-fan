package com.sildio.filmfan.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Class that allows to create the SQLite DB and initialize the tables
 *
 * Created by sildio on 9/7/18.
 */

public class SQLiteHelper extends SQLiteOpenHelper {
    private final Context context;
    public static final String DATABASE_NAME = "movies_o1.db";
    public static final String TABLE_MOVIE = "movie";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_TABLE_MOVIE = "CREATE TABLE movie ("
            + "_id integer primary key,"
            + "title text,"
            + "release_date text,"
            + "overview text,"
            + "vote_average real,"
            + "rating real,"
            + "poster_path text); ";

//Constructor
    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onOpen(SQLiteDatabase database) {

        Log.v("HD", "Database opened from MySQLite");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE_MOVIE);
        Log.v("HD", "Database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    /**
     * Function that checks if the DB has some data in it /not used for now
     *
     * @param
     * @return hasData
     */
    public boolean checkIfHasData() {
        boolean hasData = false;
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cursor = database.query(SQLiteHelper.TABLE_MOVIE,
                    new String[] { "_id" }, null, null, null, null, null);
            if (cursor.getCount() > 0)
                hasData = true;
        } catch (Exception e) {
        e.printStackTrace();

        }
        database.close();
        return hasData;
    }

    /**
     * Function to drop all the tables and recreate the DB//not used for now
     *
     * @param
     * @return
     */
    public void emptyTables() {
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            database.execSQL("DROP TABLE " + TABLE_MOVIE);

        } catch (Exception e) {
            Log.v("HD", "Problem deleting tables" + e);
        }
        try {
            database.execSQL(CREATE_TABLE_MOVIE);
        } catch (Exception e) {
            Log.v("HD", "Problem creating tables" + e);
        }
        Log.v("HD", "Database created");
        database.close();
    }
}