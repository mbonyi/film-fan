package com.sildio.filmfan.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import android.util.Log;
/**
 * Class that holds the functions to call the server (GET, POST)
 *
 * Created by sildio on 9/5/18.
 */

public class CoreApiHelper {
    public static int SUCCESS_CODE_GET = 200;
    public static int SUCCESS_CODE_POST = 201;


    /**
     * Function that sends a POST request to the server and returns a JSON response
     *
     * @param URL
     * @param data (json data)
     * @return stringBuilder
     *
     */
    public String getJsonByUrlPOST(String URL, JSONObject data) {
        StringBuilder stringBuilder = new StringBuilder();
        if(data!=null) {
           // Log.d("JSON to send: ", data.toString());
        }
        try {
            URL url = new URL(URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(30000);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestProperty("Connection", "Keep-Alive");;
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if(data!=null) {
                OutputStream os = conn.getOutputStream();
                os.write(data.toString().getBytes());
                os.flush();
            }
            if (conn.getResponseCode() != SUCCESS_CODE_POST) {
                Log.d("Error Response", conn.getErrorStream()+" status");
                BufferedReader reader1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String line;
                while ((line = reader1.readLine()) != null) {
                    stringBuilder.append(line);
                }
               // Log.d("JSON error", stringBuilder.toString());
                throw new RuntimeException(
                        "Failed : HTTP error code : " + conn.getResponseCode() + "  instead of " + SUCCESS_CODE_POST);
            } else {
                BufferedReader reader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                //Log.d("JSON reponse", stringBuilder.toString());
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
            Log.d("readJSONFeed", e.getMessage());
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * Function that sends a GET request to the server and returns a JSON response
     *
     * @param URL
     * @return stringBuilder
     *
     */
    public String getJsonByUrlGET(String URL) {
        StringBuilder stringBuilder = new StringBuilder();
       // Log.d("The url", URL);
        try {
            URL url = new URL(URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(30000);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setDoOutput(false);
            conn.setRequestMethod("GET");
            conn.connect();
            if (conn.getResponseCode() != SUCCESS_CODE_GET) {

               // Log.d("Error Response", conn.getResponseMessage()+" status");
                BufferedReader reader1 = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
                String line;
                while ((line = reader1.readLine()) != null) {
                    stringBuilder.append(line);
                }
                Log.d("JSON error", stringBuilder.toString());
               /* throw new RuntimeException(
                        "Failed : HTTP error code : " + conn.getResponseCode() + "  instead of " + SUCCESS_CODE);*/
            } else {
                BufferedReader reader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
               // Log.d("JSON reponse", stringBuilder.toString());
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
            Log.d("readJSONFeed", e.getMessage());
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

}