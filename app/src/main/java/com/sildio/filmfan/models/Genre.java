package com.sildio.filmfan.models;

/**
 * Genre model
 *
 * Created by sildio on 9/5/18.
 */

public class Genre {
    private long id;
    private  String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
