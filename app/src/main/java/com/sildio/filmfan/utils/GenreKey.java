package com.sildio.filmfan.utils;

/**
 * Server keys to help getting the Genre information from the server
 *
 * Created by sildio on 9/5/18.
 */

public class GenreKey {
    public static String CORE_API_GENRE_ID="id";
    public static String CORE_API_GENRE_NAME="name";
}
