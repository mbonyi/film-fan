package com.sildio.filmfan.utils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Class that holds all the functions about the calendar/ 1 function for now
 *
 * Created by sildio on 9/10/18.
 */


public class DateUtil {

    /**
     * Function to get the year from the string date
     *
     * @param dateInString
     * @return year
     */
    public static int getYearFromDate (String dateInString) {
        int year=1917;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date date = formatter.parse(dateInString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            year = calendar.get(Calendar.YEAR); //get the year

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return year;
    }
}
