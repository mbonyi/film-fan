package com.sildio.filmfan.utils;

/**
 * Server keys to help getting the Movie information from the server
 *
 * Created by sildio on 9/5/18.
 */

public class MovieKey {
    public static String CORE_API_MOVIE_ID="id";
    public static String CORE_API_MOVIE_TITLE="title";
    public static String CORE_API_MOVIE_OVERVIEW="overview";
    public static String CORE_API_MOVIE_RELEASE_DATE="release_date";
    public static String CORE_API_MOVIE_VOTE_AVERAGE="vote_average";
    public static String CORE_API_MOVIE_VOTE_COUNT="vote_count";
    public static String CORE_API_MOVIE_ORIGINAL_LANGUAGE="original_language";
    public static String CORE_API_MOVIE_GENRES="genres";
    public static String CORE_API_MOVIE_POSTER_PATH="poster_path";
    public static String CORE_API_MOVIE_VALUE="value";


}
