package com.sildio.filmfan.utils;

import com.sildio.filmfan.BuildConfig;

/**
 * URL values
 *
 * Created by sildio on 9/5/18.
 */

public class CoreUrlKeys {
    public static String CORE_API_URL_PATH = BuildConfig.CORE_API_URL; // API url
    public static String CORE_API_URL_PATH_AUTHENTICATION = CORE_API_URL_PATH + "/authentication";
    public static String CORE_API_URL_PATH_MOVIE = CORE_API_URL_PATH + "/movie";
    public static String IMAGE_URL_PATH = BuildConfig.IMAGE_URL_PATH;
}
