package com.sildio.filmfan.utils;

/**
 * Server keys (global)
 *
 * Created by sildio on 9/5/18.
 */

public class CoreApiKeys {
    public static String CORE_API_STATUS_CODE="status_code";
    public static String CORE_API_STATUS_MESSAGE="status_message";
    public static String CORE_API_KEY="api_key";
    public static String CORE_API_GUEST_SESSION_ID="guest_session_id";
    public static String CORE_API_RESULTS="results";
}
