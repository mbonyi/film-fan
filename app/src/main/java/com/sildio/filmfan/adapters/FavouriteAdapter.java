package com.sildio.filmfan.adapters;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.sildio.filmfan.R;
import com.sildio.filmfan.activities.FavouritesActivity;
import com.sildio.filmfan.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.sildio.filmfan.utils.CoreUrlKeys.IMAGE_URL_PATH;

/**
 * Layout adapter for the favourite movies (Layout of the movie item and action when one item is clicked)
 *
 * Created by sildio on 9/5/18.
 */

public class FavouriteAdapter extends ArrayAdapter<Movie> {
    private final Context context;
    private final ArrayList<Movie> values;
    FavouritesActivity activity;
    public FavouriteAdapter(Context context, ArrayList<Movie> values) {
        super(context, R.layout.layout_movie_item, values);
        this.context = context;
        this.values = values;
        this.activity=(FavouritesActivity) context;
    }

    /**
     * Function that returns the movie item view
     *
     * @param position
     * @param convertView
     * @param parent
     * @return view
     *
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater
                .inflate(R.layout.layout_movie_item, parent, false);

        TextView txtTitle = (TextView) view.findViewById(R.id.txt_movie_title);
        TextView txtReleaseDate = (TextView) view.findViewById(R.id.txt_release_date);
        TextView txtVoteAverage = (TextView) view.findViewById(R.id.txt_vote_avg);
        ImageView icon=(ImageView) view.findViewById(R.id.img_poster);
        txtTitle.setText(values.get(position).getTitle());
        txtReleaseDate.setText(values.get(position).getReleaseDate());
        txtVoteAverage.setText(values.get(position).getVoteAverage()+"");
        Picasso.with(context).load(IMAGE_URL_PATH+values.get(position).getPosterPath()).placeholder(R.mipmap.movie_placeholder).into(icon);
        final int p=position;

        //action to open the details in a popup when the item is clicked
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.showMovieDialog(values.get(p));
            }
        });
        return view;
    }



}