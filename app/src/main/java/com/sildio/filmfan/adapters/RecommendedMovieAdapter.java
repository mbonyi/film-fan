package com.sildio.filmfan.adapters;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sildio.filmfan.R;
import com.sildio.filmfan.activities.RecommendedMoviesActivity;
import com.sildio.filmfan.db.MovieManager;
import com.sildio.filmfan.models.Movie;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.sildio.filmfan.utils.CoreUrlKeys.IMAGE_URL_PATH;

/**
 * Layout adapter for the recommended movies (Layout of the movie item and action when one item is clicked)
 *
 * Created by sildio on 9/5/18.
 */


public class RecommendedMovieAdapter extends ArrayAdapter<Movie> {
    private final Context context;
    private final ArrayList<Movie> values;
    RecommendedMoviesActivity activity;
    public RecommendedMovieAdapter(Context context, ArrayList<Movie> values) {
        super(context, R.layout.layout_movie_item, values);
        this.context = context;
        this.values = values;
        this.activity=(RecommendedMoviesActivity) context;
    }

    /**
     * Function that returns the movie item view
     *
     * @param position
     * @param convertView
     * @param parent
     * @return view
     *
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater
                .inflate(R.layout.layout_movie_item, parent, false);

        TextView txtTitle = (TextView) view.findViewById(R.id.txt_movie_title);
        TextView txtReleaseDate = (TextView) view.findViewById(R.id.txt_release_date);
        TextView txtVoteAverage = (TextView) view.findViewById(R.id.txt_vote_avg);
        ImageView icon=(ImageView) view.findViewById(R.id.img_poster);
        Button btnSaveToFavourite = (Button) view.findViewById(R.id.btn_save_to_favourites);
        btnSaveToFavourite.setVisibility(View.VISIBLE);
        btnSaveToFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MovieManager movieManager =new MovieManager(context);
                movieManager.open();
                if(!movieManager.exists(values.get(position).getId())) {
                    movieManager.create(values.get(position));
                    Toast.makeText(context, "Movie " + values.get(position).getTitle() + " saved to your favourites!", Toast.LENGTH_LONG).show();
                }
                else
                    Toast.makeText(context, "Movie " + values.get(position).getTitle() + " was already saved to your favourites!", Toast.LENGTH_LONG).show();
                movieManager.close();


            }
        });
        txtTitle.setText(values.get(position).getTitle());
        txtReleaseDate.setText(values.get(position).getReleaseDate());
        txtVoteAverage.setText(values.get(position).getVoteAverage()+"");
        Picasso.with(context).load(IMAGE_URL_PATH+values.get(position).getPosterPath()).placeholder(R.mipmap.movie_placeholder).into(icon);
        final int p=position;

        //action to show the movie item details in a custom popup
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               activity.showMovieDialog(values.get(p));
            }
        });
        return view;
    }



}